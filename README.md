# **WebPack**

### Estructura de directorios:

* ###### src: Codigo fuente a empaquetar
* ###### dist: Codigo a dsitribuir.

### Funcionamiento:

Al ejecutarse, webpack busca en la carpeta src, y empaqueta 
ese contenido , junto con sus dependencias , en el archivo
main.js en la carpeta dist, que se puede invocar desde alli.
